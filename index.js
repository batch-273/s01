/*
  [Quiz]
  1. By declaring an array through let or const
  2. arr[0], by using the first index which is 0
  3. arr[arr.length-1], by determining the size of the array and subtracting 1
  4. arr.indexOf("item")
  5. arr. forEach(function)
  6. arr.map(function)
  7. arr.always(function)
  8. arr.some(function)
  9. True
  10. False
  
*/

let students = ["John", "Joe", "Jane", "Jessie"];

console.log("[Item 1]");

function addToEnd(arrName, name) {
  if (typeof name === "string") {
    arrName.push(name);
    console.log(arrName);
  } else {
    console.log("error - can only add strings to an array");
  }
}

addToEnd(students, "Ryan");
addToEnd(students, 045);

// [Item 2]

console.log("[Item 2]");
function addToStart(arrName, name) {
  if (typeof name === "string") {
    arrName.unshift(name);
    console.log(arrName);
  } else {
    console.log("error - can only add strings to an array");
  }
}

addToStart(students, "Tess");
addToStart(students, 033);

// [Item 3]
console.log("[Item 3]");
function elementChecker(arrName, name) {
  if (arrName[0] != undefined) {
    if (arrName.includes(name)) {
      console.log(true);
    } else {
      console.log(false);
    }
  } else {
    console.log("error - passed in array is empty");
  }
}

elementChecker(students, "Jane");
elementChecker([], "Jane");

console.log("[Item 4]");

function checkAllStringEndings(arrName, name) {
  //console.log(arrName.some((item) => typeof item !== "string"));

  if (arrName[0] == undefined) {
    console.log("error - array must not be empty");
  } else if (arrName.some((item) => typeof item !== "string")) {
    console.log("error - all array elements must be strings");
  } else if (typeof name !== "string") {
    console.log("error - 2nd argument must be of datatype of string");
  } else if (name.length > 1) {
    console.log("error - 2nd argument must be single character");
  } else {
    if (
      arrName.every((item) => {
        item[name.length] === name;
      })
    ) {
      console.log(true);
    } else {
      console.log(false);
    }
  }
}

checkAllStringEndings(students, "e");
checkAllStringEndings([], "e");
checkAllStringEndings(["Jane", 2], "e");
checkAllStringEndings(students, "el");
checkAllStringEndings(students, 4);

console.log("[Item 5]");

function stringLengthSorter(arrName) {
  console.log(
    arrName.sort(function (a, b) {
      return a.length - b.length;
    })
  );
}

stringLengthSorter(students);

console.log("[Item 6]");

function startsWithCounter(arrName, name) {
  if (arrName[0] == undefined) {
    console.log("error - array must not be empty");
  } else if (arrName.some((item) => typeof item !== "string")) {
    console.log("error - all array elements must be strings");
  } else if (typeof name !== "string") {
    console.log("error - 2nd argument must be of datatype of string");
  } else if (name.length > 1) {
    console.log("error - 2nd argument must be single character");
  } else {
    let numOfNames = 0;
    arrName.forEach((element) => {
      if (element[0] === name) {
        numOfNames += 1;
      }
    });
    console.log(numOfNames);
  }
}

startsWithCounter(students, "J");
